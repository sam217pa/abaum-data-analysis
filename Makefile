BCFS = $(shell find i -name '*.bcf.gz')
VCFS = $(BCFS:%bcf.gz=%vcf.gz)
GBKS = $(shell find i -name '*.gbk')
FASTAS = $(GBKS:%gbk=%fasta)
GFFS   = $(GBKS:%gbk=%gbk.gff)


#- ARCHIVE -----------------------------------------------------------
OUTPUTS := \
	report/report.pdf report/report.docx report/fig01.pdf	\
	report/fig02.pdf
ARCHIVE := .bkp/analyses_$(shell date +%Y-%m-%d).zip

.PHONY: archive
archive: $(ARCHIVE)

$(ARCHIVE): $(OUTPUTS) ; zip -u $@ $^

#- REPORT ------------------------------------------------------------
.PHONY: report
report: report/report.pdf report/report.docx

report/report.pdf: analysis/plot.pdf analysis/aye.pdf
report/report.docx: analysis/plot.pdf analysis/aye.pdf
report/fig01.pdf: analysis/plot.pdf ; cp $< $@
report/fig02.pdf: analysis/aye.pdf  ; cp $< $@

%.pdf: %.md
	pandoc -i $< -o $@ \
		--template eisvogel \
		--listings \
		--filter pandoc-citeproc \
		--bibliography "$$HOME/Dropbox/bibliography/references.json" \
		--csl "$$HOME/Dropbox/bibliography/styles/chicago-author-date.csl"


%.docx: %.md
	pandoc -i $< -o $@ \
		--filter pandoc-citeproc \
		--bibliography "$$HOME/Dropbox/bibliography/references.json" \
		--csl "$$HOME/Dropbox/bibliography/styles/chicago-note-bibliography-with-ibid.csl"

.PHONY: clean-report
clean-report:
	rm -f report/report.pdf report/report.docx

#- FIGURES -----------------------------------------------------------
.PHONY: figures
figures: analysis/markers.pdf analysis/aye.pdf

analysis/markers.pdf:
	Rscript analysis/compile_plot.R

analysis/aye.pdf:
	Rscript analysis/tracts-aye.r


#- VCFS --------------------------------------------------------------
.PHONY: vcfs
vcfs: $(VCFS)

#- REFERENCES --------------------------------------------------------
.PHONY: refs gffs fastas
refs: gffs fastas
gffs: $(GFFS)
fastas: $(FASTAS)

%.vcf.gz: %.bcf.gz  ; bcftools convert -Oz -o $@ $<
%.gbk.gff: %.gbk    ; bp_genbank2gff3 $< -o $(dir $@)
%.fasta: %.gbk  ; seqret -sequence $< -outseq $@
