library(samuer)
library(data.table)
library(ggplot2)
library(patchwork)
library(ggnewscale)
library(fastmatch)
library(VariantAnnotation)
library(transformr)
library(extrafont)
library(GenomicAlignments)
library(ggbio)

## global type of arrow
haro <- arrow(angle = 30, length = unit(2, "mm"), type = "closed")

## global color palette
PAL <- RColorBrewer::brewer.pal(8, "Dark2")

#- FUNCTIONS ----------------------------------------------------------

## better formatting of Megabases
format_tenth_power <- function(x) {
  nbr <- scales::label_number_auto()(x)
  nbr <- gsub("e\\+06", " Mb", nbr)
  nbr
}

## vcf accessors
gt <- function(x, ...) geno(x, ...)$GT
`gt<-`  <- function(x, value, ...) {
  geno(x, ...)$GT <- value
  x
}

## access tn slots (tract number)
tn <- function(x, j=NULL) {
  o <- geno(x)$TN
  if (!is.null(j)) {
    o[, j]
  } else {
    o
  }
}

## keep convertion tracts of sample
tr <- function(x, .sample) {
  x[!is.na(tn(x, .sample)), .sample]
}

scale_x_sequnit <- function(unit = c("Mb", "kb", "bp"), append = NULL, ...) {
  unit <- match.arg(unit)
  if (is.null(append)) {
    scale_x_continuous(
      breaks = scales::trans_breaks(
        ggbio:::trans_seq(unit),
        ggbio:::trans_seq_rev(unit)),
      labels = scales::trans_format(
        ggbio:::trans_seq_format(unit),
        scales::math_format(.x)),
      ...)
  } else {
    stopifnot(is.character(append))
    scale_x_continuous(
      labels = scales::trans_format(
        ggbio:::..append_unit(append),
        scales::math_format(.x)),
      ...)
  }
}

point <- function(size) size / ggplot2::.pt
