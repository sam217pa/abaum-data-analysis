source("functions.r")

#- READ DATA ----------------------------------------------------------

## read gff with location of genes
gff <- import("../i/GCA_000069245.1_AYE.prokka_DONOR2.gbk.gff")
dna <- readDNAStringSet("../i/twgt-donor2/ref/donor.fa")
sl <- width(dna)

vcf_path <- "../i/twgt-donor2/tracts/samples.vcf.gz"
if (!file.exists(vcf_path))
  system("bcftools convert -Oz -o ../i/twgt-donor2/tracts/samples.vcf.gz ../i/twgt-donor2/tracts/samples.bcf.gz")
d <- readVcf(vcf_path)

colnames(d) <- stringr::str_split_fixed(colnames(d), "_", 2)[, 1]
header(d)@samples <- colnames(d)
## check sample names
sampleNames(d)

## read recip/donor variants
donor_variants <- readVcf("../i/twgt-donor2/bcf/donor-targets.vcf")
## keep only SNV
donor_variants <- donor_variants[isSNV(donor_variants), ]
## convert to data.table
dv_pos <- data.table(
  pos = start(donor_variants),
  sname = "donor")

par(mfrow = c(1, 1))
plot(table(rowSums(apply(gt(d), 2, "!=", "."))))

#--- plot_distr_converted_markers

par(
  mfrow=c(length(colnames(d)) / 2 + 1, 2),
  mar = c(1, 2, 3, 2),
  cex.main = 0.7)
dpos <- rbindlist(map(colnames(d), function(x) {
  s <- start(d[gt(d)[, x] == "0" , x])
  hist(s, breaks = 200, xlim = c(0, sl),
       main = x, xlab = "", ylab = "")
  data.table(sname = x, pos = s)
}))

#-- Conversion Tract --------------------------------------------------

## position of converted markers for each sample
dpos <- rbindlist(lapply(colnames(d), function(x) {
  a <- tr(d, x)
  a <- data.table(sname = x, pos = start(a), tn = as.vector(tn(a)))
  a
}))

## location of conversion tracts
dtn <- dpos[, .(s = min(pos), e = max(pos)), by = .(sname, tn)]

ggplot(dtn, aes(x = s, xend = e, y = sname, yend = sname)) +
  geom_segment(size = 3)

cds <- gff[gff$type == "CDS"]
abar4_range <- gff[gff$type == "mobile_element"]
abar4_range <- range(abar4_range) + 5e4
abar4_range

gff_abar <- subsetByOverlaps(gff, abar4_range, type = "within", ignore.strand = TRUE)


#-- Bam Coverage ------------------------------------------------------

abar_samples <- sampleNames(d)
covs <- (function(.) {
  # read indexed bam file
  bams <- do.call(c, lapply(sampleNames(d), function(x) {
    list.files(
      "../i/twgt-donor2/bam",
      pattern = paste0("donor_", x),
      full.names = TRUE)
  }))
  df <- data.frame(
    sname = abar_samples, bampath = bams[-grep(".bai$", bams)])
  sbp <- ScanBamParam(which = abar4_range, what = scanBamWhat())
  df$bam <- lapply(df$bampath, readGAlignmentPairs, param = sbp)
  df$covs <- lapply(df$bam, function(bam) {
    wdws <- slidingWindows(abar4_range, width = 500, step=250)[[1]]
    wdws$depth <- vapply(coverage(bam)[wdws], mean, numeric(1))
    wdws
  })
  df
})()

covdf <- rbindlist(apply(covs, 1, function(x) {
  df <- as.data.table(x[["covs"]])
  df$sname <- x[["sname"]]
  df
}))
.h <- setNames(seq_along(abar_samples), abar_samples)
## covdf$depth <- covdf$depth / 100
covdf[, `:=`(sdepth, scale(depth)), by = sname]
covdf$sdepth <- covdf$sdepth / 30
covdf$sdepth <- (.h[covdf$sname] + covdf$sdepth)
cov_range <- covdf[, .(
  start = min(start),
  end = max(end),
  max = max(sdepth),
  min = min(sdepth)),
  by = sname]

#-- Abar Screen -------------------------------------------------------

abar_island <- gff_abar[gff_abar$type == "mobile_element"]

containment_abar_island <- local({
  v <- Views(dna[[1]], ranges(abar_island))
  v <- setNames(v, "abar_island")
  r1 <- list.files("../i/twgt-donor2/fq", pattern = "*_1.*.fq.gz", full.names = TRUE)
  r2 <- list.files("../i/twgt-donor2/fq", pattern = "*_2.*.fq.gz", full.names = TRUE)
  map2(r1, r2, function(f, r) {
    mashscreenr::Screen(v, f, r)
  })
})

names(containment_abar_island) <-
  unlist(lapply(lapply(names(containment_abar_island), basename), function(x) {
    strsplit(x, "_")[[1]][1]
  }))
containment_abar_island <- lfdf(containment_abar_island)
containment_abar_island[, rate_shared := hashes_shared / hashes_total]


kmer_width <- 1e4
shared_kmer <- containment_abar_island[id %in% sampleNames(d), .(id, rate_shared)]
shared_kmer[, y := .h[id]]
shared_kmer[, x0 := end(abar4_range)]
shared_kmer[, x  := end(abar4_range) + kmer_width * rate_shared]
shared_kmer_grid <- end(abar4_range) + kmer_width * c(0, 0.25, 0.5, 0.75, 1)

#- PLOT CONVERSION TRACTS ---------------------------------------------

plot_conv <- ggplot(
  dpos[pos >= start(abar4_range) & pos <= end(abar4_range)]
, aes(x = pos, y = sname)) +
  theme_void(
    base_family = "Lato Light") +
  theme(
    axis.text = element_text(family = "Lato Light", size = 8),
    axis.text.x = element_blank(),
    axis.title.x = element_blank(),
    plot.margin = margin(0, 5, 0, 5),
    panel.grid.major = element_line(
      size = 0.2, color = "gray", linetype = "dotted")) +
  geom_segment(
    data = dtn[s >= start(abar4_range) & e <= end(abar4_range)],
    aes(yend = sname, x = s, xend = e),
    color = "gray",
    size = 3) +
  geom_point(
    shape = 25,
    fill = PAL[3],
    color = PAL[3],
    size = 0.5,
    alpha = 0.1,
    position = position_nudge(y = 0.1)) +
  geom_rect(
    data = cov_range,
    aes(x = NULL,
        xmin = start, xmax = end,
        ymin = min,   ymax = max,
        group = sname),
    fill = gray(0.7),
    alpha = 0.2,
    position = position_nudge(y = +0.3)) +
  geom_line(
    data = covdf,
    aes(
      x = (start + end) / 2,
      y = sdepth,
      group = sname),
    size = 0.2,
    col = PAL[5],
    position = position_nudge(y = +0.3)) +
  geom_text(
    data = covdf[start == min(start), .(depth, sdepth), by = sname],
    aes(x = start(abar4_range),
        y = sdepth,
        label = round(depth, digits = 0)),
    size = point(5),
    hjust = 1,
    position = position_nudge(y = +0.4),
    family = "Lato Light") +
  geom_text(
    data = cov_range,
    aes(label = "0", y = min, x = start),
    size = point(5),
    hjust = 1,
    family = "Lato Light",
    position = position_nudge(y = + 0.3)) +
  geom_point(
    data = data.table(
      sname = "markers",
      pos = start(subsetByOverlaps(
        donor_variants, abar4_range, type = "within"))),
    aes(y = 0.5),
    shape = 24,
    alpha = 0.1,
    color = PAL[2],
    fill = PAL[2],
    size = 0.5) +
  annotate(
    "text",
    x = start(abar4_range),
    y = 0.9,
    label = "Donor/recipient markers",
    hjust = 0,
    vjust = 1,
    family = "Lato Light",
    size = point(7),
    color = PAL[2]) +
  annotate(
    "text",
    x = start(abar4_range),
    y = 1.4,
    label = "Read coverage",
    hjust = 0,
    vjust = 0,
    family = "Lato Light",
    size = point(7),
    color = PAL[5]) +
  annotate(
    "text",
    x = start(abar4_range) + 5e3,
    y = 2,
    label = "Converted markers",
    hjust = 0,
    vjust = 0,
    family = "Lato Light",
    size = point(7),
    color = PAL[3]) +
  scale_x_sequnit() +
  xlim(start(abar4_range), end(abar4_range) + kmer_width + 1) +
  geom_vline(
    xintercept = shared_kmer_grid,
    size = 0.1,
    color = gray(0.6)) +
  ggplot2::geom_rect(
    data = shared_kmer,
    aes(x = NULL, y = NULL, xmin = x0, xmax = x, ymin = y-0.05, ymax = y+0.05),
    fill = PAL[1]) +
  ggplot2::geom_text(
    data = shared_kmer,
    aes(label = paste0(rate_shared * 100, "%"), y = y, x = x),
    family = "Lato Light",
    size = point(5),
    hjust = -0.1) +
  annotate(
    geom = "text",
    x = end(abar4_range),
    y = 1.2,
    label = "AbaR shared\nk-mer",
    color = PAL[1],
    hjust = 0,
    vjust = 0,
    family = "Lato Light",
    size = point(5)
  )

#-- Plot Genes --------------------------------------------------------

abar_cds <- subsetByOverlaps(
  gff, abar4_range,
  type = "within",
  ignore.strand = TRUE)
abar_cds <- as.data.table(abar_cds)
abar_cds$end_ <- ifelse(
  abar_cds$strand == "-",
  abar_cds$start,
  abar_cds$end
)
abar_cds$start_ <- ifelse(
  abar_cds$strand == "-",
  abar_cds$end,
  abar_cds$start
)
abar_cds$color <- ifelse(abar_cds$strand == "+", gray(0.6), gray(0.8))
abar_cds[grep("ComM", product), c("color", "over") := .(PAL[1], TRUE)]
abar_cds[grep("IS", product), c("color", "over") := .(PAL[6], TRUE)]
abar_cds[grep("lactamase", product), c("color", "over") := .(PAL[4], TRUE)]
abar_cds[grep("Aminoglycoside 3'-phospho", product), c("color", "over") := .(PAL[7], TRUE)]

gr <- GRanges(abar_cds[, .(seqnames, start, end, strand, type, color, over)])
gr$over[is.na(gr$over)] <- FALSE
gr$y <- 0

annots <- abar_cds[over == TRUE]
annots$y <- (c(-1.5, 1.5, 2.5, 2, 1.5, 1.5, -1.5, 1.5, 2, 1.5, -1.5)) / 4
annots$product <-
  c("comM", "ISPst9", "IS26", "neo1", "IS15", "bla", "per1", "IS10A",
    "neo2", "ISPst9", "comM")
annots[, x := (start + end) / 2]

plot_genes <-
  ggplot(gr[gr$type == "CDS"]) +
  geom_segment(
    data = gr[gr$type == "mobile_element"],
    aes(y = 0),
    size = 6,
    alpha = 0.2,
    color = PAL[3],
    stat = "identity") +
  geom_arrow(
    aes(color = color, y = y), stat = "identity",
    size = 2,
    linejoin = "mitre",
    type = "closed") +
  geom_arrow(
    data = gr[gr$over],
    aes(color = color, y = y), stat = "identity",
    size = 2,
    linejoin = "mitre",
    type = "closed") +
  geom_text(
    data = annots,
    aes(x = x,
        y = y,
        label = product,
        color = color),
    family = "Lato Light",
    size = point(5),
    hjust = 0.5) +
  ggbio::scale_x_sequnit() +
  xlim(start(abar4_range), end(abar4_range) + kmer_width + 1) +
  scale_color_identity() +
  expand_limits(y = c(-0.5, 0.5)) +
  guides(
    size = "none",
    color = "none",
    fill = "none") +
  theme_void(
    base_family = "Lato Light") +
  theme(
    axis.text = element_text(family = "Lato Light", size = 8),
    axis.text.y = element_blank(),
    panel.grid.major.x = element_line(size = 0.2, color = "gray", linetype = "dotted"),
    plot.margin = margin(0, 5, 0, 5))

#- SAVE TO PDF --------------------------------------------------------

cairo_pdf("./aye.pdf", family = "Lato Light", width = 7.25, height=7.25/2)
(plot_conv / plot_genes@ggplot) + plot_layout(heights = c(5, 1))
dev.off()
embed_fonts("./aye.pdf")
