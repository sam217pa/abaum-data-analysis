source("functions.r")

#- READ DATA ----------------------------------------------------------

## read gff with location of genes
gff <- import("../i/40288_nanopore_illumina_DONOR1.gbk.gff")

## read DNA of donor genome
dna <- readDNAStringSet("../i/twgt-donor1/ref/donor.fa")
## sequence length of donor genome
sl <- width(dna)


## read position of tracts on donor
d <- readVcf("../i/twgt-donor1/tracts/samples.vcf.gz")
colnames(d) <- stringr::str_split_fixed(colnames(d), "_", 2)[, 1]
header(d)@samples <- colnames(d)
## remove strain AB11
d <- d[, -which(sampleNames(d) %in% "AB11")]
## check sample names
sampleNames(d)

## read metadata
mdat <- fread("../etc/Description_files.csv")

## read recip/donor variants
donor_variants <- readVcf("../i/twgt-donor1/bcf/donor-targets.vcf")
## keep only SNV
donor_variants <- donor_variants[isSNV(donor_variants), ]
## convert to data.table
dv_pos <- data.table(
  pos = start(donor_variants),
  sname = "donor")

#--- plot_high_freq_locus

par(mfrow = c(1, 1), mar = c(4, 5, 5, 4))
plot(table(rowSums(apply(gt(d), 2, "!=", "."))))

#--- remove high freq locus
d <- d[rowSums(gt(d) != ".") < length(colnames(d)), ]

#--- plot_distr_converted_markers

par(
  mfrow=c(length(colnames(d)) / 2 + 1, 2),
  mar = c(1, 2, 3, 2),
  cex.main = 0.7)
dpos <- rbindlist(map(colnames(d), function(x) {
  s <- start(d[gt(d)[, x] == "0" , x])
  hist(s, breaks = 200, xlim = c(0, sl),
       main = x, xlab = "", ylab = "")
  data.table(sname = x, pos = s)
}))

#-- Conversion Tract --------------------------------------------------

## position of converted markers for each sample
dpos <- rbindlist(lapply(colnames(d), function(x) {
  a <- tr(d, x)
  a <- data.table(sname = x, pos = start(a), tn = as.vector(tn(a)))
  a
}))

## location of conversion tracts
dtn <- dpos[, .(s = min(pos), e = max(pos)), by = .(sname, tn)]


#- ABAR ---------------------------------------------------------------

## sample 26 seems to have converted abar4 while it should not?
abar4_samples <- c(mdat[grep("AbaR4", sample_name), short_name], "AB26")
abar4_samples <- abar4_samples[-which(abar4_samples == "AB11")]
vgrg_samples <- mdat[grep("vgrG", sample_name), short_name]
vgrg_samples <- vgrg_samples[-which(vgrg_samples == "AB26")]

abar4_pos <- dpos[sname %in% abar4_samples]
abar4_range <- GRanges(
  unique(seqnames(d)), IRanges(min(abar4_pos$pos), max(abar4_pos$pos)))

abar4_cds <- as.data.frame(
  subsetByOverlaps(gff[gff$type == "CDS"], abar4_range,
                   type = "within"))
abar4_cds <- as.data.table(abar4_cds)
abar4_cds$end_ <- ifelse(
  abar4_cds$strand == "-",
  abar4_cds$start,
  abar4_cds$end)

#-- Kmer Containment --------------------------------------------------

gff_over_abar4_range <- subsetByOverlaps(gff, abar4_range, type="within", ignore.strand = TRUE)
inter_comM <- range(gff_over_abar4_range[grep("ComM", gff_over_abar4_range$product)])
abar_island <- range(gff_over_abar4_range[gff_over_abar4_range$type == "mobile_element"][1])
abar_fqs <- lapply(abar4_samples, function(x) {
  list.files(
    "../i/twgt-donor1/fq",
    pattern = x,
    full.names = TRUE)
})

containment_inter_comM <- local({
  v <- Views(dna[[1]], ranges(inter_comM))
  v <- setNames(v, "inter_comM")
  lapply(abar_fqs, function(fr) {
    mashscreenr::Screen(v, fr[1], fr[2])
  })
})

names(containment_inter_comM) <- abar4_samples
containment_inter_comM <- lfdf(containment_inter_comM)
containment_inter_comM[, rate_shared := hashes_shared / hashes_total]

kmer_width <- 5e3
shared_kmer <- containment_inter_comM[id %in% abar4_samples, .(id, rate_shared)]
shared_kmer[, y := .h[id]]
shared_kmer[, x0 := end(abar4_range)]
shared_kmer[, x  := end(abar4_range) + kmer_width * rate_shared]
shared_kmer_grid <- end(abar4_range) + kmer_width * c(0, 0.25, 0.5, 0.75, 1)

#-- bam coverage -------------------------------------------------------

covs <- (function(.) {
  # read indexed bam file
  bams <- do.call(c, lapply(abar4_samples, function(x) {
    list.files(
      "../i/twgt-donor1/bam",
      pattern = paste0("donor_", x),
      full.names = TRUE)
  }))
  df <- data.frame(
    sname = abar4_samples, bampath = bams[-grep(".bai$", bams)])
  sbp <- ScanBamParam(which = abar4_range, what = scanBamWhat())
  df$bam <- lapply(df$bampath, readGAlignmentPairs, param = sbp)
  df$covs <- lapply(df$bam, function(bam) {
    wdws <- slidingWindows(abar4_range, width = 500, step=250)[[1]]
    wdws$depth <- vapply(coverage(bam)[wdws], mean, numeric(1))
    wdws
  })
  df
})()

covdf <- rbindlist(apply(covs, 1, function(x) {
  df <- as.data.table(x[["covs"]])
  df$sname <- x[["sname"]]
  df
}))
.h <- setNames(seq_along(abar4_samples), abar4_samples)
## covdf$depth <- covdf$depth / 100
covdf[, `:=`(sdepth, scale(depth)), by = sname]
covdf$sdepth <- covdf$sdepth / 30
covdf$sdepth <- (.h[covdf$sname] + covdf$sdepth)
cov_range <- covdf[, .(
  start = min(start),
  end = (max(end)),
  max = max(sdepth),
  min = (min(sdepth))),
  by = sname]

abar4_transposon <- abar4_cds[
  grep("Beta-lactamase|IS4 family|ComM", product),
  .(start, end_, strand, product)]
abar4_transposon$color <- PAL[c(1, 6, 6, 4, 6, 6, 1)]
abar4_transposon$product <- gsub(
  "IS4 family ", "", abar4_transposon$product)
abar4_transposon$product <- gsub(
  "Competence protein ", "", abar4_transposon$product)
abar4_transposon$product[
  grep("Beta", abar4_transposon$product)] <- "β-lactamase"

abar4_transposon$y <- (c(2, 8, 9, 0, 0, -1, 2) + 1) / 3
abar4_cds$color <- ifelse(abar4_cds$strand == "+", gray(0.6), gray(0.8))

x_breaks <- c(3.8e6, 3.85e6, 3.9e6)
abar4_x_vline <- abar4_transposon[c(1, 4, 7), ]
abar4_x_vline$x <- abar4_x_vline[, (start + end_) / 2]

#- PLOT VGRG ---------------------------------------------------------------

vgrg_pos <- dpos[sname %in% vgrg_samples]

cds <- gff[gff$type == "CDS"]
vgrg_range <- GRanges(unique(seqnames(d)), IRanges(7.0e4, 180e3))
vgrg_cds <- as.data.frame(subsetByOverlaps(
  gff[gff$type == "CDS"], vgrg_range, type = "within"))
vgrg_cds <- as.data.table(vgrg_cds)
vgrg_cds$end_ <- ifelse(
  vgrg_cds$strand == "-", vgrg_cds$start, vgrg_cds$end)
vgrg_pos <- vgrg_pos[
  pos >= min(start(vgrg_range)) & pos <= max(end(vgrg_range)) ]
vgrg_dtn <- dtn[
  sname %in% vgrg_samples][
    s >= min(start(vgrg_range)) & e <= max(end(vgrg_range))]

vgrg_covs <- (function(.) {
  # read indexed bam file
  bams <- do.call(c, lapply(vgrg_samples, function(x) {
    list.files("../i/twgt-donor1/bam", pattern = paste0("donor_", x), full.names = TRUE)
  }))
  df <- data.frame(sname = vgrg_samples, bampath = bams[-grep(".bai$", bams)])
  sbp <- ScanBamParam(which = vgrg_range, what = scanBamWhat())
  df$bam <- lapply(df$bampath, readGAlignmentPairs, param = sbp)
  df$covs <- lapply(df$bam, function(bam) {
    wdws <- slidingWindows(vgrg_range, width = 500, step=250)[[1]]
    wdws$depth <- vapply(coverage(bam)[wdws], mean, numeric(1))
    wdws
  })
  df
})()


vgrg_covdf <- rbindlist(apply(vgrg_covs, 1, function(x) {
  df <- as.data.table(x[["covs"]])
  df$sname <- x[["sname"]]
  df
}))
vgrg_h <- setNames(seq_along(vgrg_samples), vgrg_samples)
vgrg_covdf[, `:=`(sdepth, scale(depth)), by = sname]
vgrg_covdf$sdepth <- vgrg_covdf$sdepth / 30
vgrg_covdf$sdepth <- (vgrg_h[vgrg_covdf$sname] + vgrg_covdf$sdepth)
vgrg_cov_range <- vgrg_covdf[, .(start = min(start), end = (max(end)), max = max(sdepth), min = (min(sdepth))), by = sname]

vgrg_transposon <- vgrg_cds[48:54, .(start, end_, strand, product)][-c(3, 4)]
vgrg_transposon$color <- PAL[c(6, 6, 4, 6, 6)]
vgrg_transposon$product <- gsub("IS4 family ", "", vgrg_transposon$product)
vgrg_transposon$product <- gsub("Competence protein ", "", vgrg_transposon$product)
vgrg_transposon$product[grep("Beta", vgrg_transposon$product)] <- "β-lactamase"
vgrg_transposon$y <- c(3, 3.5, 2, -1, -1.5) + 2
vgrg_cds$color <- ifelse(vgrg_cds$strand == "+", gray(0.6), gray(0.8))
vgrg_x_vline <- vgrg_transposon[c(1, 3, 5), ]
vgrg_x_vline$x <- vgrg_x_vline[, (start + end_) / 2]
vgrg_x_breaks = c(9e4, 12e4, 18e4)


gff_over_vgrg_range <- subsetByOverlaps(gff, vgrg_range, type="within", ignore.strand = TRUE)
vgrg_island <- range(gff_over_vgrg_range[gff_over_vgrg_range$type == "mobile_element"][1]) + 1e3
vgrg_fqs <- lapply(vgrg_samples, function(x) {
  list.files(
    "../i/twgt-donor1/fq",
    pattern = x,
    full.names = TRUE)
})

containment_vgrg_island <- local({
  v <- Views(dna[[1]], ranges(vgrg_island))
  v <- setNames(v, "vgrg_island")
  lapply(vgrg_fqs, function(fr) {
    mashscreenr::Screen(v, fr[1], fr[2])
  })
})

names(containment_vgrg_island) <- vgrg_samples
containment_vgrg_island <- lfdf(containment_vgrg_island)
containment_vgrg_island[, rate_shared := hashes_shared / hashes_total]

vgrg_shared_kmer <- containment_vgrg_island[id %in% vgrg_samples, .(id, rate_shared)]
vgrg_shared_kmer[, y := vgrg_h[id]]
vgrg_shared_kmer[, x0 := end(vgrg_range)]
vgrg_shared_kmer[, x := end(vgrg_range) + kmer_width * rate_shared]
vgrg_shared_kmer_grid <- end(vgrg_range) + kmer_width * c(0, 0.25, 0.5, 0.75, 1)

#- GLOBAL DISTRIBUTION ------------------------------------------------

plot_tracts <-
  ggplot(copy(dtn)[, `:=`(sname = factor(
    sname, levels = c(abar4_samples, vgrg_samples)))][],
    aes(x = s, xend = e, y = sname, yend = sname)) +
  geom_segment(size = 3, color = gray(0.3)) +
  scale_x_sequnit() +
  theme_void(base_family = "Lato Light") +
  theme(
    axis.text = element_text(size = 8),
    axis.text.y = element_blank(),
    axis.text.x = element_text(),
    axis.title.x = element_blank(),
    plot.margin = margin(0, 5, 0, 5),
    panel.grid.major.y = element_blank(),
    panel.grid.major.x = element_line(linetype = "dotted", size = 0.2),
    panel.background = element_rect(fill = gray(0.95)))
