---
title: Analysis of transformation events in co-cultures of Acinetobacter sp.
author: samuel barreto
date: 2020-05-03T16:22:00+0200
nofoot: true
---

# Objectives

1. Describe the location of transformation events between strains 40288 and M2 at two insertion locus: either the insertion of transposon Tn2006 in the vgrG locus, involved in Type VI secretion system, or the insertion of AbaR island disrupting comM involved (somehow?) in natural competence [@GodeuxCuringLargeResistance2020].
2. Idem with transformation events between strains AYE and M2, at comM insertion of island AbaR4.

# Data

Short reads sequencing of whole-genome extraction of colonies screened for insertions using adequate AB; donor is not competent; recipient is Rif-resistant.
Short reads:  (150 bp, 300bp insertion length).

- Recipient M2 genome as a genbank file, Prokka-annotated.
- Donor 1 (40288) idem.
- Donor 2 (AYE) idem.

See file [etc/Description\_files.csv](/Users/samuelbarreto/projets/aa_transfo-co-culture_2020-04-01/etc/Description_files.csv) for data summary.

With donor 1, two sites are of interest (vgrG and comM)
The location of insertions were PCR-confirmed (but downwards analysis suggests it is not 100% sensitive).

# Methods

### Variant-calling and transformation events location identification
Sequencing reads were subsampled to match an estimated 50x coverage of donor genome using *seqtk* version 1.3-r106 [@LiSeqtk2020].
Reads of transformants were mapped to their corresponding donor genome using *bwa mem* version 0.7.17-r1888 [@LiFastAccurateShort2009],
and sorted using *samtools* version 1.10 [@LiStatisticalFrameworkSNP2011].
Donor and recipient genome short reads were simulated using *wgsim* version 1.10 [@LiWgsim2020], and mapped to the donor genome using the same parameters as for transformant sequencing reads.
Reads were then tallied on donor genome using *bcftools* version 1.10, to keep only sites with read coverage above 25x, where haplotype differs between donor and recipient, and match donor haplotype.
These variant calling and identification of converted markers were performed using *twgt* version 0.1.0 [@BarretoTWGT2020].
The resulting VCF file was then parsed using *VariantAnnotation* from the Bioconductor framework version 3.10, using R version 3.6 [@ObenchainVariantAnnotationBioconductorPackage2014; @HuberOrchestratingHighthroughputGenomic2015; @RCoreTeamLanguageEnvironmentStatistical2020].
Data analysis pipeline and analysis scripts are available at <https://gitlab.com/sam217pa/abaum-data-analysis>.

### Confirmation of insertions by read screening

Tn2006 and AbaR4 being absent from recipient genome, they cannot be detected from single nucleotide polymorphisms and short insertions/deletions.
Their insertions in the transformant genome was thus confirmed via read screening:
Their sequence together with 1 kbp of flanking sequence was sketched using *mash sketch* with default parameters; the presence of this sketches in sequencing reads was confirmed using *mash screen* version 2.2.2 [@OndovMashFastGenome2016;@OndovMashScreenHighthroughput2019].

# Results

### Insertions of 40288 Tn2006 in vgrG and AbaR in comM

![Insertions of Tn2006 at vgrG locus (top panel) and AbaR at comM locus (bottom panel).](/Users/samuelbarreto/projets/aa_transfo-co-culture_2020-04-01/analysis/plot.pdf)

### Insertions of AYE AbaR in comM

![Insertions of AbaR at comM locus](/Users/samuelbarreto/projets/aa_transfo-co-culture_2020-04-01/analysis/aye.pdf)

# References
